FROM d4science/r-full:latest

LABEL org.d4science.image.licenses="EUPL-1.2" \
      org.d4science.image.source="https://code-repo.d4science.org/gCubeSystem/docker-r-full" \
      org.d4science.image.vendor="D4Science <https://www.d4science.org>" \
      org.d4science.image.authors="Andrea Dell'Amico <andrea.dellamico@isti.cnr.it>, Roberto Cirillo <roberto.cirillo@isti.cnr.it>"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y fuse zip jq openjdk-8-jdk-headless

ENV PANDOC_VERSION=default
RUN /rocker_scripts/install_pandoc.sh
